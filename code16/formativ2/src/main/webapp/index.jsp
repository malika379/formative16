<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<head>
<title>Formative 1</title>
<meta charset="ISO-8859-1">
<style>
.wrapper {
	width: 70%;
	text-align: center;
	margin: auto;
	width: 50%;
	border: 3px solid;
	padding: 10px;
	text-align: left;
}

h1 {
	text-align: center;
}

.input_field {
	display: flex;
	flex-direction: row;
	justify-content: left;
	margin-bottom: 10px;
}

.label {
	width: 30%;
}

.input, .input_disabled {
	width: 70%;
}

#service, #total {
	text-align: right;
}

#assurance {
	width: 5%;
	text-align: left;
}

#total, #service {
	background-color: #d3d3d3;
}

.male, .female, .input_a {
	display: flex;
	flex-direction: row;
	width: 50%;
	float: left;
}

#male-label, #female-label, #male, #female {
	display: flex;
	flex-direction: row;
	width: 50%;
}

input, select, textarea, option {
	display: flex;
	flex-direction: row;
	width: 100%;
}

textarea {
	height: 100px;
}

.button {
	display: flex;
	justify-content: center;
	align-items: center;
}
</style>
</head>
<body>
	<div class="wrapper">
		<h1>Sending address</h1>
		<form action="<%=request.getContextPath()%>/addAddress"
			method="post">
			<div class="input_field">
				<div class="label">
					<label for="name">Recipient</label>
				</div>
				<div class="input">
					<input type="text" id="name" name="name"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<span>Gender</span>
				</div>
				<div class="input">
					<div class="male">
						<input type="radio" id="male" name="gender" value="male">
						<label id="male-label" for="male">Male</label>
					</div>
					<div class="female">
						<input type="radio" id="female" name="gender" value="female">
						<label id="female-label" for="female">Female</label>
					</div>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="">Street</label>
				</div>
				<div class="input">
					<input type="text" id="street" name="street"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="">Zip Code</label>
				</div>
				<div class="input">
					<input type="text" id="zip" name="zip"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="">District</label>
				</div>
				<div class="input">
					<input type="text" id="district" name="district"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="">City</label>
				</div>
				<div class="input">
					<input type="text" id="city" name="city"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="">Province</label>
				</div>
				<div class="input">
					<input type="text" id="province" name="province"></input>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="country">Country</label>
				</div>
				<div class="input">
					<select name="country" id="country">
						<option selected="true" disabled="disabled">Please Select</option>
						<option value="Indonesia">Indonesia</option>
						<option value="Indonesia">Singapore</option>
						<option value="Indonesia">Malaysia</option>
						<option value="Indonesia">Australia</option>
					</select>
				</div>
			</div>

			<div class="input_field">
				<div class="label">
					<span>Assurance? (10%)</span>
				</div>
				<div class="input_a">
					<input type="checkbox" id="assurance" name="assurance"
						value="Assurance" onclick="myFunction()"></input> <label for="assurance">Checkbox</label>
				</div>
			</div>
			<div class="input_field">
				<div class="label"></div>
				<div class="input">
					<textarea id="desc" name="desc"></textarea>
				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="total">Total Price</label>
				</div>
				<div class="input_disabled">
					<input type="text" id="total" style='pointer-events: none;'
						readonly="readonly" name="total" value="RP 1.250.000,-"></input>

				</div>
			</div>
			<div class="input_field">
				<div class="label">
					<label for="service">Delivery Service Cost</label>
				</div>
				<div class="input_disabled">
					<input type="text" style='pointer-events: none;' id="service"
						readonly="readonly" name="service" value="RP 50.000,-"></input>
				</div>
			</div>
			<div class="button">
				<button>Send</button>
			</div>
		</form>
	</div>
	<script>
        function myFunction(){
            var c = document.getElementById("assurance");
            if (c.checked == true) {
                document.getElementById("total").value = "Rp 1.375.000,00";
            } else{
                document.getElementById("total").value = "Rp 1.250.000,00";
            }
        }
    </script>
</body>
</html>