package id.bootcamp.formative2;

public class Address {
	private String name;
	private String gender;
	private String street;
	private int zip;
	private String district;
	private String city;
	private String province;
	private String country;
	private boolean assurance;
	private String desc;
	private int price;
	private int service;
	public Address() {}
	
	public Address(String name, String gender, String street, int zip, String district, String city, 
				String province,String country, boolean assurance, String desc, int price, int service) {
		this.name = name;
		this.gender = gender;
		this.street = street;
		this.zip = zip;
		this.district = district;
		this.city = city;
		this.province = province;
		this.country = country;
		this.assurance = assurance;
		this.desc = desc;
		this.price = price;
		this.service = service;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isAssurance() {
		return assurance;
	}

	public void setAssurance(boolean assurance) {
		this.assurance = assurance;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getService() {
		return service;
	}

	public void setService(int service) {
		this.service = service;
	}
}
