package id.bootcamp.formative2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/addAddress")
public class AddressServlet extends HttpServlet{
private static final long serialVersionUID = 1L;
    
	private AddressDAO addressDao = new AddressDAO();

    public AddressServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String street = request.getParameter("street");
		String sZip = request.getParameter("zip");
		String district = request.getParameter("district");
		String city = request.getParameter("city");
		String province = request.getParameter("province");
		String country = request.getParameter("country");
		String sAssurance=request.getParameter("assurance");
		String desc = request.getParameter("desc");
		String sPrice = request.getParameter("total");
		String sService =  request.getParameter("service");
		sZip =  sZip.replaceAll("[^0-9]", "");
		sPrice =  sPrice.replaceAll("[^0-9]", "");
		sService =  sService.replaceAll("[^0-9]", "");
		int price = Integer.parseInt(sPrice);
		int service = Integer.parseInt(sService);
		int zip = Integer.parseInt(sZip);
		boolean assurance=false;
		
		if(sAssurance!=null) {
			assurance=true;
		}
		
		Address address = new Address();
		address.setName(name);
		address.setGender(gender);
		address.setStreet(street);
		address.setZip(zip);
		address.setDistrict(district);
		address.setCity(city);
		address.setProvince(province);
		address.setCountry(country);
		address.setAssurance(assurance);
		address.setDesc(desc);
		address.setPrice(price);
		address.setService(service);
		try {
			addressDao.addAddress(address);
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		
		response.sendRedirect("");
	}
}
