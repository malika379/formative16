package id.bootcamp.formative2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddressDAO {
	public int addAddress(Address user) throws ClassNotFoundException {
		String INSERT_USER_SQL = "INSERT INTO address"
				+ "(name, gender, street, zip, district, city, province, country, assurance, description, price, service) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?);";
		int result = 0;
		Class.forName("com.mysql.cj.jdbc.Driver");
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/formative16", "root",
				"allahuakbar!99"); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getGender());
			preparedStatement.setString(3, user.getStreet());
			preparedStatement.setInt(4, user.getZip());
			preparedStatement.setString(5, user.getDistrict());
			preparedStatement.setString(6, user.getCity());
			preparedStatement.setString(7, user.getProvince());
			preparedStatement.setString(8, user.getCountry());
			preparedStatement.setBoolean(9, user.isAssurance());
			preparedStatement.setString(10, user.getDesc());
			preparedStatement.setInt(11, user.getPrice());
			preparedStatement.setInt(12, user.getService());
			System.out.println(preparedStatement);
			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
