package com.bootcamp.formative3.controller;

import com.bootcamp.formative3.model.Address;
import com.bootcamp.formative3.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.OpenOption;
import java.util.List;
import java.util.Optional;

@RestController
public class AddressController {
    @Autowired
    private AddressRepository repo;

    @GetMapping("/getAddressById/{id}")
    public Address getAddressById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @GetMapping("/getAll")
    public List<Address> getAll(){
        return repo.findAll();
    }
    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable(value = "id")int id){
        repo.deleteById(id);
        return "Data dengan id " + id + " telah dihapus";
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Address> update(@RequestBody Address address, @PathVariable(value = "id") int id){
        Optional<Address> addressOptional = Optional.ofNullable(this.repo.findById(id));
        if(!addressOptional.isPresent())
            return  ResponseEntity.notFound().build();
        address.setId(id);
        this.repo.save(address);
        return ResponseEntity.noContent().build();
    }
}
