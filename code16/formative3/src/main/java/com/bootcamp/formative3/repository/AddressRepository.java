package com.bootcamp.formative3.repository;

import com.bootcamp.formative3.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository  extends CrudRepository<Address, Integer> {
    List<Address> findAll();
    Address findById(int id);
    void deleteById(int id);
    Address save(Address address);
}
