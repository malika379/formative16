package com.bootcamp.formative3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative3Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative3Application.class, args);
	}

}
